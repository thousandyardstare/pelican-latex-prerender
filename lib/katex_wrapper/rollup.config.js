import commonjs from "rollup-plugin-commonjs";
import nodeResolve from "rollup-plugin-node-resolve";
import builtins from 'rollup-plugin-node-builtins';
import { terser } from "rollup-plugin-terser";
export default {
  input      : "main.js",
  output     : {
    file: "bundle.js",
    name: "sheeshKebab",
    format     : "iife",
  },
  plugins    : [
    terser(),

    builtins(),

    commonjs({
      include: [ "./main.js", "node_modules/**" ],
      sourceMap: false
    }),

    nodeResolve({
      jsnext: true,
      main: false
    })
  ]
};