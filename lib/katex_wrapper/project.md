* [x] barebones project w/ mathjax
* [x] export inline latex as html-string
* [x] standalone tool tex-string -> html-string
* [ ] write pelican plugin that goes through markdown and finds equations
* [ ] hook pelican plugin to standalone tool
* [ ] replace equations with html-str

Newly popped up:

* [ ] crop latex in CLI-tool
* [x] check for inline/display