import unittest
from tempfile import NamedTemporaryFile

from pelican.readers import RstReader

from latex_prerender.latex_prerender import rst_process_html
from latex_prerender.katex_convert import convert_latex2html
from latex_prerender.latex_markdown_extension import InlineLatexProcessor
from latex_prerender.latex_markdown_extension import BlockLatexProcessor


class TestKatexConvert(unittest.TestCase):
    def test_display_latex(self):
        latex = convert_latex2html(r"\frac{123}{456}")

        self.assertIn('<span class="katex-display">', latex)
        self.assertIn('<span class="mfrac">', latex)

    def test_inline_latex(self):
        latex = convert_latex2html(r"\frac{123}{456}", display=False)

        self.assertNotIn('<span class="katex-display">', latex)
        self.assertIn('<span class="mfrac">', latex)


class TestRST(unittest.TestCase):
    def setUp(self):
        self.reader = RstReader(
            settings={
                "FORMATTED_FIELDS": [],
                "DOCUTILS_SETTINGS": {"math_output": "LaTeX"},
            }
        )

    def test_display(self):
        rst_string = """.. math::
   \\frac{123}{456}"""

        with NamedTemporaryFile(mode="w") as tmp_rst_file:
            tmp_rst_file.write(rst_string)
            tmp_rst_file.flush()

            html, _ = self.reader.read(tmp_rst_file.name)
            latex_str = rst_process_html(html)

            self.assertIn('<span class="katex-display">', latex_str)
            self.assertIn('<span class="mfrac">', latex_str)

    def test_inline(self):
        rst_string = r":math:`\frac{123}{456}`"

        with NamedTemporaryFile(mode="w") as tmp_rst_file:
            tmp_rst_file.write(rst_string)
            tmp_rst_file.flush()

            html, _ = self.reader.read(tmp_rst_file.name)
            latex_str = rst_process_html(html)

            self.assertNotIn('<span class="katex-display">', latex_str)
            self.assertIn('<span class="mfrac">', latex_str)


class MockMd:
    def __init__(self):
        self.tab_length = None


class MockParser:
    def __init__(self):
        self.md = MockMd()


class TestMarkdownProcessors(unittest.TestCase):
    def setUp(self):
        self.mock_parser = MockParser()
        self.inline_processor = InlineLatexProcessor(self.mock_parser)
        self.display_processor = BlockLatexProcessor(self.mock_parser)

    # Inline Math

    def test_inline_negative(self):
        self.assertFalse(self.inline_processor.test(None, "$$ a = b $$"))

    def test_inline_positive(self):
        self.assertTrue(self.inline_processor.test(None, "And therefore $a = b$"))
        self.assertTrue(self.inline_processor.test(None, r"And therefore \(a = b\)"))

    # Display Math

    def test_display_positive(self):
        dollar_test = ["$$a=b$$"]
        bracket_test = [r"\[a=b\]"]

        self.assertTrue(self.display_processor.test(None, dollar_test[0]))
        self.assertTrue(self.display_processor.test(None, bracket_test[0]))

        self.display_processor.run(None, dollar_test)
        self.display_processor.run(None, bracket_test)

        html_conversion_test_str = (
            '<annotation encoding="application/x-tex">a=b</annotation>'
        )
        self.assertIn(html_conversion_test_str, dollar_test[0])
        self.assertIn(html_conversion_test_str, bracket_test[0])

    def test_display_negative(self):
        self.assertFalse(self.display_processor.test(None, "And therefore $a = b$"))


if __name__ == "__main__":
    unittest.main()
