"""
Latex-Prerender: Render your equations directly into HTML. No JS at runtime.
KaTex > MathJax, you're welcome.
"""

import re
import markdown

from .katex_convert import convert_latex2html


class BlockLatexProcessor(markdown.blockprocessors.BlockProcessor):
    def __init__(self, parser):
        super().__init__(parser)

    def test(self, parent, block):
        return bool(block.startswith("$$")) or bool(block.startswith(r"\["))

    def run(self, parent, blocks):
        latex_str = blocks.pop(0)
        latex_parts = re.match(r"(\$\$|\\\[)([\s\S]*?)(\$\$|\\\])", latex_str)
        latex_inner = latex_parts.group(2)

        latex_html = convert_latex2html(latex_inner)

        blocks.insert(0, latex_html)


class InlineLatexProcessor(markdown.blockprocessors.BlockProcessor):
    def __init__(self, parser):
        super().__init__(parser)

        self.inline_pattern = r"(\$|\\\()(.+?)(\\\)|\$)"

    def test(self, parent, block):
        contains_latex = re.search(self.inline_pattern, block, flags=re.MULTILINE)
        is_block_latex = bool(block.startswith("$$")) or bool(block.startswith(r"\["))

        return not is_block_latex and contains_latex

    def run(self, parent, blocks):
        latex_str = blocks.pop(0)

        regex = re.compile(self.inline_pattern, flags=re.MULTILINE)

        replace_with_latex = lambda mo: convert_latex2html(mo.group(2), display=False)

        latex_html = regex.sub(replace_with_latex, latex_str)

        blocks.insert(0, latex_html)


class MarkdownKatexExtension(markdown.Extension):
    def __init__(self, config):
        super(MarkdownKatexExtension, self).__init__()

    def extendMarkdown(self, md, md_globals):
        display_latex = BlockLatexProcessor(md.parser)
        inline_latex = InlineLatexProcessor(md.parser)

        md.parser.blockprocessors.register(display_latex, "latex_prerender_display", 50)
        md.parser.blockprocessors.register(inline_latex, "latex_prerender_inline", 50)
