import subprocess
import os


def convert_latex2html(latex_str, display=True):
    command = ["node", "../lib/katex_wrapper/bundle.js"]

    if not display:
        command.append("-i")

    command.append(latex_str)

    try:
        completed_process = subprocess.run(
            command,
            check=True,
            cwd=os.path.dirname(os.path.realpath(__file__)),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
    except subprocess.CalledProcessError as e:
        print(
            "[latex_prerender] Error, failed during processing of -->{}<-- "
            "(Exception: {}). Continuing with empty output".format(latex_str, e)
        )
        print("Command used: {}".format(command))
        return ""
    except FileNotFoundError as e:
        print("[latex_prerender] Error, do you have node installed? (And in your PATH)")
        raise e

    return completed_process.stdout.decode("utf-8")
