"""
Latex-Prerender: Render your equations directly into HTML. No JS at runtime.
KaTex > MathJax, you're welcome.
"""

import os
import re
import copy

try:
    from html import unescape  # python 3.4+
except ImportError:
    try:
        from html.parser import HTMLParser  # python 3.x (<3.4)
    except ImportError:
        from HTMLParser import HTMLParser  # python 2.x
    unescape = HTMLParser().unescape

from pelican import signals

try:
    from .latex_markdown_extension import MarkdownKatexExtension
except ImportError as e:
    print(
        "\nGot following exception: {}. Maybe markdown is not installed, if so, math only works in reStructuredText.\n".format(
            e
        )
    )
    MarkdownKatexExtension = None
from .katex_convert import convert_latex2html


re_rst_math_inline = re.compile(r'<tt class="math">(.+?)</tt>', re.MULTILINE)
re_rst_math_block = re.compile('<pre class="math">\n(.+?)\n</pre>', re.MULTILINE)


def pelican_init(pelican_obj):
    settings = pelican_obj.settings.setdefault("DOCUTILS_SETTINGS", {})
    settings["math_output"] = "LaTeX"

    config = {}
    if MarkdownKatexExtension is not None:
        pelican_obj.settings["MARKDOWN"].setdefault("extensions", []).append(
            MarkdownKatexExtension(config)
        )


def rst_process_html(html_string):
    def r_inline(match):
        formula = unescape(match.group(1))
        return convert_latex2html(formula, display=False)

    def r_block(match):
        formula = unescape(match.group(1))
        return convert_latex2html(formula, display=True)

    html_string = re_rst_math_inline.sub(r_inline, html_string)
    html_string = re_rst_math_block.sub(r_block, html_string)

    return html_string


def rst_add_latex(instance):
    _, ext = os.path.splitext(os.path.basename(instance.source_path))
    if ext != ".rst":
        return
    if 'class="math"' not in instance._content:
        return

    instance._content = rst_process_html(instance._content)


def register():
    signals.initialized.connect(pelican_init)
    signals.content_object_init.connect(rst_add_latex)
